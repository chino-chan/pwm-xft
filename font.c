/*
 * pwm/font.c
 *
 * Copyright (c) Tuomo Valkonen 1999-2001. 
 *
 * You may distribute and modify this program under the terms of either
 * the Clarified Artistic License or the GNU GPL, version 2 or later.
 */

#include <libtu/output.h>
#include <libtu/misc.h>
#include <string.h>
#include "font.h"
#include "config.h"
#include "global.h"

int __XFT_TEXT_WIDTH(XFontStruct *fs, const char *str, int len)
{
	XGlyphInfo ext;
	XftTextExtentsUtf8(wglobal.dpy, XFT_GLOBAL_FONT, str, len, &ext);
	return ext.width;
}


XFontStruct *load_font(Display *dpy, const char *fontname)
{
	XFontStruct *xfnt;
	
	xfnt=XLoadQueryFont(dpy, fontname);
	
	if(xfnt==NULL){
		warn("Could not load font \"%s\", trying \"%s\"",
			 fontname, CF_FALLBACK_FONT_NAME);
		xfnt=XLoadQueryFont(dpy, CF_FALLBACK_FONT_NAME);
		if(xfnt==NULL){
			warn("Failed loading fallback font.");
			return FALSE;
		}
	}
	
	return xfnt;
}


static char *scatn3(const char *p1, int l1,
					const char *p2, int l2,
					const char *p3, int l3)
{
	char *p=ALLOC_N(char, l1+l2+l3+1);
	
	if(p==NULL){
		warn_err();
	}else{
		strncat(p, p1, l1);
		strncat(p, p2, l2);
		strncat(p, p3, l3);
	}
	return p;
}


char *make_label(XFontStruct *fnt, const char *str, const char *trailer,
				 int maxw, int *wret)
{
	static const char *dots="...";
	const char *b;
	size_t len, tlen;
	int w, bw, dw,  tw;
	
	len=strlen(str);
	tlen=strlen(trailer);
	
	w=__XFT_TEXT_WIDTH(fnt, str, len);
	tw=__XFT_TEXT_WIDTH(fnt, trailer, tlen);
	
	if(tw+w<=maxw){
		if(wret!=NULL)
			*wret=tw+w;
		return scat(str, trailer);
	}
	
	b=strchr(str, ':');
	
	if(b!=NULL){
		b++;
		b+=strspn(b, " ");
		bw=__XFT_TEXT_WIDTH(fnt, str, b-str);
		
		if(tw+w-bw<=maxw){
			if(wret!=NULL)
				*wret=tw+w-bw;
			return scat(b, trailer);
		}
		
		len-=(b-str);
		str=b;
		w=bw;
	}

	dw=__XFT_TEXT_WIDTH(fnt, dots, 3);
	
	if(len>0){
		while(--len){
			w=__XFT_TEXT_WIDTH(fnt, str, len);
			if(tw+w+dw<=maxw)
				break;
		}
	}
	
	if(wret!=NULL)
		*wret=tw+w+dw;
	
	
	return scatn3(str, len, dots, 3, trailer, tlen);
}

